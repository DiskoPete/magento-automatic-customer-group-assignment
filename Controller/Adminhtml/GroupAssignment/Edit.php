<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Controller\Adminhtml\GroupAssignment;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;

class Edit extends Action implements HttpGetActionInterface
{

    public function execute()
    {
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $this->configurePage($page);

        return $page;
    }

    private function configurePage(Page $page): void
    {
        $page->setActiveMenu('DiskoPete_AutomaticCustomerGroupAssignment::customer_group_assignment');
        $page->getConfig()->getTitle()->prepend(__('Edit Customer Group Assignment'));
        $page->addBreadcrumb(__('Customers'), __('Customers'));
        $page->addBreadcrumb(__('Customer Group Assignment'), __('Edit Customer Group Assignment'));
    }
}
