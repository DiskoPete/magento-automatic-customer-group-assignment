<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Controller\Adminhtml\GroupAssignment;

use DiskoPete\AutomaticCustomerGroupAssignment\Model\Assignment\AssignmentRepository;
use DiskoPete\AutomaticCustomerGroupAssignment\Model\AssignmentFactory;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface;

class Save extends Action implements HttpPostActionInterface
{
    /**
     * @var AssignmentFactory
     */
    private $assignmentFactory;
    /**
     * @var AssignmentRepository
     */
    private $assignmentRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        LoggerInterface                             $logger,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        AssignmentFactory                           $assignmentFactory,
        AssignmentRepository                        $assignmentRepository,
        Action\Context                              $context
    )
    {
        parent::__construct($context);
        $this->assignmentFactory    = $assignmentFactory;
        $this->assignmentRepository = $assignmentRepository;
        $this->messageManager       = $messageManager;
        $this->logger               = $logger;
    }


    public function execute()
    {

        try {
            $assignment = $this->assignmentFactory->create();
            $assignment->setData($this->_request->getParam('general'));
            $this->assignmentRepository->save($assignment);
            $this->messageManager->addSuccessMessage(__('Saved assignment'));
        } catch (\Throwable $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(__('An error occurred while saving the assignment'));
        }


        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $this->configureRedirect($redirect);

        return $redirect;
    }

    private function configureRedirect(Redirect $redirect): void
    {
        $redirect->setPath('*/groupassignment');
    }
}
