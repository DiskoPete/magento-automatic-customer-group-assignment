<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Controller\Adminhtml\GroupAssignment;

use DiskoPete\AutomaticCustomerGroupAssignment\Model\Assignment\AssignmentRepository;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Message\ManagerInterface;
use Psr\Log\LoggerInterface;
use Throwable;

class Delete extends Action implements HttpPostActionInterface
{
    /**
     * @var AssignmentRepository
     */
    private $assignmentRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        LoggerInterface      $logger,
        ManagerInterface     $messageManager,
        AssignmentRepository $assignmentRepository,
        Action\Context       $context
    )
    {
        parent::__construct($context);
        $this->assignmentRepository = $assignmentRepository;
        $this->messageManager       = $messageManager;
        $this->logger               = $logger;
    }


    public function execute()
    {
        try {
            $id = $this->_request->getParam('id');
            $this->assignmentRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('Deleted assignment'));
        } catch (Throwable $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(__('Could not delete assignment'));
        }

        return $this->_redirect('*/*');

    }
}
