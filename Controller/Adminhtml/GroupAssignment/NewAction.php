<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Controller\Adminhtml\GroupAssignment;

use DiskoPete\AutomaticCustomerGroupAssignment\Block\Adminhtml\Assignment\Edit;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class NewAction extends Action implements HttpGetActionInterface
{

    public function execute()
    {
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $this->configurePage($page);

        return $page;
    }

    private function configurePage(Page $page): void
    {
        $page->setActiveMenu('DiskoPete_AutomaticCustomerGroupAssignment::customer_group_assignment');
        $page->getConfig()->getTitle()->prepend(__('New Customer Group Assignment'));
        $page->addBreadcrumb(__('Customers'), __('Customers'));
        $page->addBreadcrumb(__('Customer Group Assignment'), __('New Customer Group Assignment'));
    }
}
