<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Block\Adminhtml\Assignment;

use Magento\Backend\Block\Widget\Form\Container;
use Magento\Customer\Controller\RegistryConstants;

class Edit extends Container
{
    /**
     * Update Save and Delete buttons. Remove Delete button if group can't be deleted.
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_group_assignment';
        $this->_blockGroup = 'Magento_Customer';

        $this->buttonList->update('save', 'label', __('Save Assignment'));
        $this->buttonList->update('delete', 'label', __('Delete Assignment'));

//        $groupId = $this->coreRegistry->registry(RegistryConstants::CURRENT_GROUP_ID);
//        if (!$groupId || $this->groupManagement->isReadonly($groupId)) {
//            $this->buttonList->remove('delete');
//        }
    }

    /**
     * Retrieve the header text, either editing an existing group or creating a new one.
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        return 'Foobar';
//        $groupId = $this->coreRegistry->registry(RegistryConstants::CURRENT_GROUP_ID);
//        if ($groupId === null) {
//            return __('New Customer Group');
//        } else {
//            $group = $this->groupRepository->getById($groupId);
//            return __('Edit Customer Group "%1"', $this->escapeHtml($group->getCode()));
//        }
    }

    /**
     * Retrieve CSS classes added to the header.
     *
     * @return string
     */
    public function getHeaderCssClass()
    {
        return 'icon-head head-customer-groups';
    }
}
