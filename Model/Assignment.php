<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Model;


use Magento\Framework\Model\AbstractModel;

/**
 * @method int getCustomerGroupId()
 */
class Assignment extends AbstractModel
{
    protected $_eventPrefix = 'diskopete_automaticcustomergroupassignment_assigment';

    protected function _construct()
    {
        $this->_init(ResourceModel\Assignment::class);
    }
}
