<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Model\ResourceModel\Assignment;

use DiskoPete\AutomaticCustomerGroupAssignment\Model\Assignment;
use DiskoPete\AutomaticCustomerGroupAssignment\Model\ResourceModel\Assignment as AssignmentResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        parent::_construct();

        $this->_init(
            Assignment::class,
            AssignmentResourceModel::class
        );
    }
}
