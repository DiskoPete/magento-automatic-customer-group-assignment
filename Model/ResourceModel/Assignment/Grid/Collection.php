<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Model\ResourceModel\Assignment\Grid;

use DiskoPete\AutomaticCustomerGroupAssignment\Model\ResourceModel\Assignment;
use Magento\Framework\Api\Search\AggregationInterface;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;

class Collection extends Assignment\Collection implements SearchResultInterface
{

    /**
     * @var AggregationInterface
     */
    private $aggregations;

    public function setItems(array $items = null)
    {
        return $this;
    }

    public function getAggregations()
    {
        return $this->aggregations;
    }

    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
        return $this;
    }

    public function getSearchCriteria()
    {
        return null;

    }

    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria)
    {
        return $this;

    }

    public function getTotalCount()
    {
        return $this->getSize();

    }

    public function setTotalCount($totalCount)
    {
        return $this;
    }

    protected function _construct()
    {
        parent::_construct();

        $this->_init(
            Document::class, Assignment::class);
        $this->setMainTable('customer_group_assignment');
    }
}
