<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Assignment extends AbstractDb
{

    protected function _construct()
    {
        $this->_init('customer_group_assignment', 'assignment_id');
    }
}
