<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Model\Assignment\Form;

use DiskoPete\AutomaticCustomerGroupAssignment\Model\ResourceModel\Assignment\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    public function __construct(
        CollectionFactory $assignmentCollectionFactory,
                          $name,
                          $primaryFieldName,
                          $requestFieldName,
        array             $meta = [],
        array             $data = []
    )
    {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

        $this->collection = $assignmentCollectionFactory->create();
    }
}
