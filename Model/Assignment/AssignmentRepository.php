<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Model\Assignment;

use DiskoPete\AutomaticCustomerGroupAssignment\Model\Assignment;
use DiskoPete\AutomaticCustomerGroupAssignment\Model\AssignmentFactory;
use DiskoPete\AutomaticCustomerGroupAssignment\Model\ResourceModel\Assignment as ResourceModel;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

class AssignmentRepository
{
    /**
     * @var ResourceModel
     */
    private $resource;
    /**
     * @var AssignmentFactory
     */
    private $assignmentFactory;

    public function __construct(
        AssignmentFactory $assignmentFactory,
        ResourceModel     $resource
    )
    {
        $this->resource          = $resource;
        $this->assignmentFactory = $assignmentFactory;
    }

    public function save(Assignment $assignment): bool
    {
        $this->resource->save($assignment);
        return true;
    }

    public function deleteById(int $id): bool
    {
        $assignment = $this->getById($id);

        try {
            $this->resource->delete($assignment);
        } catch (\Throwable $e) {
            throw new CouldNotDeleteException(__('Could not delete assignment'), $e);
        }

        return true;
    }

    private function getById(int $id): Assignment
    {
        $assignment = $this->assignmentFactory->create();
        $this->resource->load($assignment, $id);

        if (!$assignment->getId()) {
            throw new NoSuchEntityException(__('Could not find assignment'));
        }

        return $assignment;

    }

    public function getByEmailHost(string $emailHost): Assignment
    {
        $assignment = $this->assignmentFactory->create();
        $this->resource->load($assignment, $emailHost, 'email_host');

        if (!$assignment->getId()) {
            throw new NoSuchEntityException(__('Could not find assignment'));
        }
        return $assignment;
    }

}
