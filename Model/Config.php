<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function isAssignmentRequired(): bool
    {
        return $this->scopeConfig->isSetFlag(
            'customer/create_account/customer_group_assignment_required',
            ScopeInterface::SCOPE_STORE
        );
    }

}
