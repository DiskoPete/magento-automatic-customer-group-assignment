<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'DiskoPete_AutomaticCustomerGroupAssignment', __DIR__);
