<?php

namespace DiskoPete\AutomaticCustomerGroupAssignment\Plugin;

use DiskoPete\AutomaticCustomerGroupAssignment\Model\Assignment\AssignmentRepository;
use DiskoPete\AutomaticCustomerGroupAssignment\Model\Config;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Throwable;

class AssignCustomerGroup
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var AssignmentRepository
     */
    private $assignmentRepository;
    /**
     * @var Config
     */
    private $config;

    public function __construct(
        Config               $config,
        AssignmentRepository $assignmentRepository,
        LoggerInterface      $logger
    )
    {
        $this->logger               = $logger;
        $this->assignmentRepository = $assignmentRepository;
        $this->config               = $config;
    }


    public function beforeCreateAccount(
        AccountManagementInterface $subject,
        CustomerInterface          $customer
    ): void
    {

        try {
            $assignment = $this->assignmentRepository->getByEmailHost(
                $this->extractHost($customer->getEmail())
            );

            $customer->setGroupId(
                $assignment->getCustomerGroupId()
            );

        } catch (Throwable $e) {

            if (!$e instanceof NoSuchEntityException) {
                $this->logger->critical($e);

                return;
            }

            if ($this->config->isAssignmentRequired()) {
                throw new LocalizedException(
                    __("You can't register with the given email address.")
                );
            }
        }
    }

    private function extractHost(string $email): string
    {
        return substr(
            $email,
            (strpos($email, '@') + 1)
        );
    }
}
